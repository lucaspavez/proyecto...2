DISTANCIAS DESDE EL CENTRO GEOMETRICO

Lo que se busca con este proyecto es crear un programa utilizando bash scrip,
para que a partir de un archivo PDB se logren obtener los datos deseados, de 
esta forma serán agrupados y procesados, para poder entregar el resultado obtenido. 

Este programa procesa unicamente proteínas para así poder graficar sus ligandos o 
iones, discriminados por la distancia medida en ángstrom que sera ingresada por el 
usuario para así obtener los residuos que rodean a los ligandos o iones de la 
proteina ingresada 

Para que el programa funcione es necesario que tenga acceso a internet para poder 
descargar la base de datos. También es necesario que este intalado el paquete 
graphviz (sudo apt-get install). 

Para llamar al programa se debe abrir una terminal, ubicarse en la carpeta que 
contenga el proyecto e ingresar en la terminal: bash Proyecto.sh

Al ingresar al programa se le pedira ingresar una proteina la cual sera descargada,
inmediatamente el usuario deberá ingresar la distancia. Luego el programa debería correr 
si todo fue ingresado correctamente.(el programa crea varios archivos por lo que se recomienda 
tenerlo en una carpeta especifica).

Autores 
Lucas Pavez Calleja 