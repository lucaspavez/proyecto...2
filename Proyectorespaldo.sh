#!/bin/bash 
if [[ -f "bd-pdb.txt" ]]; then
	echo ""
else
    wget https://icb.utalca.cl/~fduran/bd-pdb.txt   
    echo ""
fi

echo "----------------------------------------"
echo "--------ingresa una proteina------------"
echo "----------------------------------------"
read prote 
P=$(grep -w "$prote" bd-pdb.txt | rev | awk -F, '{ print $1 }' | rev)
#P=$(grep $prote bd-pdb.txt | awk -F "," '{print $4}')

echo $P

if [[ $P == '"Protein"' ]]; then
	wget https://files.rcsb.org/download/$prote.pdb
	awk 'BEGIN{FS=" ";OFS="\t"}($1=="ATOM"){print $2,$3,$4,$5,$6,$7,$8,$9}' $prote.pdb | \
	awk 'length($2) >= 4 {$2 = substr($2,1,3)" "substr($2,5,7)} { print $0 }' | \
	awk 'length($3) > 3 { $3 = substr($3,2,4)} { print $0 }' > $prote.txt
	awk '$1 == "HETATM" && $4 != "HOH" { print $4" "$5" "$6" "$7" "$8" "$9 }' $prote.pdb > ligando_$prote.txt
	awk -f CCG.awk ligando_$prote.txt >> hola.txt
	
		
else
	echo "No es proteina"
fi

echo $P










